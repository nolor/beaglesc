BeaglePru : UGen {

	*ar {
		| pin = 0 |
		^this.multiNew('audio', pin);
	}

	*kr {
		| pin = 0 |
		^this.multiNew('control', pin);
	}

}
